//
//  ServiceManager.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/27/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit
class ServiceManager: ApiManager {
    static var categories = [Category]()
    static func getServiceCategories(downloadCompeted: @escaping DownloadComplete<[Category]>) {
        get(url: Constants.CATEGORIES_URL) { (response) in
            if let categories = response["data"].array {
                for category in categories {
                    print(category)
                    self.categories.append(Category(id: category["id"].string!, name: category["name_ar"].string!, englishName: category["name_en"].string!, image: category["image"].string!))
                }
                downloadCompeted(self.categories)
            }
        }
    }
    
    static func requestService(main_category: String, sub_category: String, descrption: String, map_lat: Double, map_lng: Double, member_id: String, service_day: String, service_time: String, image: UIImage?, video: URL?, downloadCompeted: @escaping DownloadComplete<Any?>) {
        
        let request: [String: Any] = [
            "main_category": main_category,
            "sub_category": sub_category,
            "descrption": descrption,
            "map_lat": map_lat,
            "map_lng": map_lng,
            "member_id": member_id,
            "service_day": service_day,
            "service_time": service_time,
        ]
        
        
        get(url: Constants.REQUEST_SERVICE_ENDPOINT, method: .post, params: request) { res in
            print(res)
            guard let result = res["result"].string else {
                return
            }
            
            if result == "true" {
                print("order sent successfully")
            } else {
                
            }
        }
    }
 }
