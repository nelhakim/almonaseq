//
//  ServicesVC.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/9/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit

class ServicesVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var categories = [Category]()
    let userDef = UserDefaults.standard
    var userId: String?

    @IBOutlet weak var servicesCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        buildSideMenu()
        servicesCollection.delegate = self
        servicesCollection.dataSource = self
        servicesCollection.register(UINib(nibName:"ServiceCategoryCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCategoryCell")
        userId = userDef.string(forKey: "user_id")
    }

    override func viewWillAppear(_ animated: Bool) {
        getCategories()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCategoryCell", for: indexPath) as? ServiceCategoryCell {
            cell.configureCell(category: categories[indexPath.row])
            return cell
        } else {
           return UICollectionViewCell()
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionWidth: CGFloat = self.servicesCollection.frame.size.width
        let itemWidth: CGFloat = (collectionWidth - 20) / 3
        let itemHeight: CGFloat = itemWidth * 1.2
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("clicked")
        if userId != nil {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestVC") as? RequestVC {
                vc.userId = userId
                vc.requestType = "service"
                vc.categoryId = categories[indexPath.row].id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
//                self.navigationController?.pushViewController(vc, animated: true)
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    
    
    @IBAction func toggleMenu(_ sender: Any) {
        toggleSideMenu()
    }
    
    
    func getCategories() {
        ServiceManager.getServiceCategories { (categories) in
            self.categories = categories
            self.servicesCollection.reloadData()
        }
    }
   
}
