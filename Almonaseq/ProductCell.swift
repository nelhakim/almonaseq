//
//  ProductCell.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 1/19/18.
//  Copyright © 2018 Nada El Hakim. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {

    @IBOutlet weak var card: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        UIView.roundView(view: card, radius: 8)
    }
    
    func configureCell(product: Product) {
        name.text = product.name
        price.text = product.price
         productImage.sd_setImage(with: URL(string: product.image), placeholderImage: UIImage(named: "logo"))
    }

}
