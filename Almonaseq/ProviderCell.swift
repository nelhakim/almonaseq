//
//  ProviderCell.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/14/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

class ProviderCell: UITableViewCell {

    @IBOutlet weak var rating: SwiftyStarRatingView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var providerImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(provider: Provider) {
        rating.value = CGFloat(provider.rating)
        providerName.text = provider.englishName
        price.text = "\(provider.price)"
        if (provider.status) {
            status.text = "available"
            status.backgroundColor = UIColor.green
        } else {
            status.text = "not available"
            status.backgroundColor = UIColor.red
        }
        providerImage.image = UIImage(named: "provider")
        
    }
    
}
