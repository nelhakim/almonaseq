//
//  UserManager.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 12/17/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import Foundation
class UserManager: ApiManager {
    private static let userDef = UserDefaults.standard
    static func register(name: String, username: String, password: String, email: String, mobile: String, lat: Double, long: Double, downloadCompeted: @escaping DownloadComplete<[String: Any]>) {
        let user: [String : Any] = ["name": name, "user_name": username, "password": password, "email": email, "phone": mobile, "map_lat": lat, "map_lng": long]
        
        print(user)
        
        get(url: Constants.REGISTER_ENDPOINT, method: .post, params: user) { res in
            print(res)
            var registered: Bool
            guard let result = res["status"].string else {
                return
            }
            
            switch result {
            case "true":
                guard let id = res["id"].int else {
                    return
                }
                registered = true
                downloadCompeted(["registered": registered, "id": id])
                
            default:
                registered = false
                print("registration error")
                print(res)
                downloadCompeted(["registered": registered, "id": 0])

                guard let msg = res["msg"].string else {
                    return
                }
                Utils.showToast(message: msg)
            }
        }
    }
    
    static func login(email: String, password: String, downloadCompeted: @escaping DownloadComplete<[String: Any]>) {
        let user: [String : Any] = ["email": email, "password": password]
        
        get(url: Constants.LOGIN_ENDPOINT, method: .post, params: user) { res in
            print(res)
            guard let result = res["status"].string else {
                return
            }
            
            switch result {
            case "true":
               print("logged in")
               guard let id = res["id"].string else {
                return
               }
               downloadCompeted(["id": id])
               self.userDef.set(true, forKey: "logged_in")
               self.userDef.set(id, forKey: "user_id")
            default:
                print("login error")
                guard let msg = res["msg"].string else {
                    return
                }
                Utils.showToast(message: msg)
            }
        }
    }
    
//    static func saveUserLocally(user: User){
//        let userDictionary: [String: String] = [
//            "id": user.id,
//            "name": user.name,
//            "username": user.username,
//            "password": user.password,
//            "image": user.image,
//            "phone": user.phone,
//            "email": user.email,
//            "city": user.city
//        ]
//        self.userDef.set(userDictionary, forKey: "user")
//    }
}
