//
//  UIView+Rounding.swift
//  ZAD
//
//  Created by Manar Magdy on 8/20/16.
//  Copyright © 2016 Manar Magdy. All rights reserved.
//

import UIKit


extension UIView {
    
    class func roundView(view: UIView, radius: CGFloat) {
        view.layer.cornerRadius = radius;
        view.clipsToBounds = true;
    }
    
    class func circularView(view: UIView) {
                view.layer.masksToBounds = false
        view.layer.cornerRadius = view.frame.size.width/2
        view.clipsToBounds = true
    }
    

}
