//
//  RequestServiceVC.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 1/17/18.
//  Copyright © 2018 Nada El Hakim. All rights reserved.
//

import UIKit
import MaterialTextField
import LocationPicker
import DateTimePicker
import MobileCoreServices

class RequestVC: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var requestType: String?
    var selectedProduct: Product?
    var productAmount = 1
    let locationPicker = LocationPickerViewController()
    let imagePicker = UIImagePickerController()
    var coords: [String:Double]?
    var selectedAddres: String?
    var userId: String?
    var categoryId: String?
    var datePicker: DateTimePicker?
    var timePicker: DateTimePicker?
    var selectedDay: Date?
    var selectedDayString: String?
    var selectedTimeString: String?
    var selectedTime: Date?
    var selectedImage: UIImage?
    var selectedVideoUrl: URL?
    @IBOutlet weak var total: MFTextField!
    @IBOutlet weak var selectedVideoThumb: UIImageView!
    @IBOutlet weak var selectedImageThumb: UIImageView!
    
    @IBOutlet weak var amount: MFTextField!
    @IBOutlet weak var address: MFTextField!
    
    @IBOutlet weak var serviceDay: MFTextField!
    @IBOutlet weak var serviceDescription: MFTextField!
    
    @IBOutlet weak var serviceTime: MFTextField!
    
    @IBOutlet var addMedia: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        address.inputView = UIView()
        
        setupLocationPicker()
        selectedDay = Date()
        populateDateField(selectedDate: Date())
        amount.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }

    override func viewWillAppear(_ animated: Bool) {
        // TODO: populate address field with user saved location
        if (requestType == "product") {
            amount.text = "\(productAmount)"
            let calculated = Int(productAmount) * Int((selectedProduct?.price)!)!
            total.text = "\(calculated)"
        }
    }
    
    @IBAction func chooseLocation(_ sender: MFTextField) {
        print("choose location")
        
        locationPicker.completion = { location in
            // do some awesome stuff with location
            print("location is\(String(describing: location?.address)) - ")
            self.address.text = location?.address
            self.selectedAddres = location?.address
            self.coords = ["lat": (location?.coordinate.latitude)!, "long": (location?.coordinate.longitude)!]
            print("coords: \(String(describing: self.coords))")
        }
        
        navigationController?.pushViewController(locationPicker, animated: true)

    }
   
    
    // Setting location picker
    func setupLocationPicker() {
        locationPicker.mapType = .standard // default: .Hybrid
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (requestType == "product") {
            if 2...4 ~= indexPath.row {
                cell.isHidden = true
            }
        } else {
            if indexPath.row == 5 {
                cell.isHidden = true
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (requestType == "product") {
            if 2...4 ~= indexPath.row {
                return 0
            } else {
                return super.tableView(tableView, heightForRowAt: indexPath)
            }
        } else {
            if indexPath.row == 5 {
                return 0
            } else {
                return super.tableView(tableView, heightForRowAt: indexPath)
            }
        }
    }
    
    
    @IBAction func selectDate(_ sender: MFTextField) {
        if (sender.tag == 71) {
            print("selecting date")
            let today = Date()
            let max = Calendar.current.date(byAdding: .month, value: 1, to: today)
            datePicker = DateTimePicker.show(selected: today, minimumDate: today, maximumDate: max, timeInterval: DateTimePicker.MinuteInterval.default)

            datePicker?.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
            datePicker?.isDatePickerOnly = true // to hide time and show only date picker
            datePicker?.is12HourFormat = true
            datePicker?.includeMonth = true
            datePicker?.completionHandler = { date in
                // do something after tapping done
                self.selectedDay = date
                self.populateDateField(selectedDate: date)
            }
        }
    
    }
    
  
    
    @IBAction func selectTime(_ sender: MFTextField) {
        if (self.selectedDay != nil) {
            timePicker = DateTimePicker.show(selected: selectedDay, minimumDate: selectedDay, maximumDate: nil, timeInterval: DateTimePicker.MinuteInterval.default)
            timePicker?.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
            timePicker?.isTimePickerOnly = true // to hide time and show only date picker

            timePicker?.completionHandler = { date in
                // do something after tapping done
                print("selected date: \(date)")
                self.populateTimeField(selectedDate: date)
            }
        } else {
            print("please select day first")
        }
       
    }
    
    func formatDate(date: Date, format: String = "yyyy-MM-dd") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let formattedDate: String = dateFormatter.string(from: date)
        return formattedDate
    }
    
   
    
    func populateDateField(selectedDate: Date) {
        selectedDayString = self.formatDate(date: selectedDate)
        serviceDay.text = selectedDayString
        populateTimeField(selectedDate: selectedDate)
    }
    
    func populateTimeField(selectedDate: Date) {
        selectedTimeString = formatDate(date: selectedDate, format: "HH:mm")
        serviceTime.text = selectedTimeString
    }
    
    func requestService() {
        if validateForm() {
            ServiceManager.requestService(main_category: categoryId!, sub_category: "5", descrption: "test", map_lat: coords!["lat"]!, map_lng: coords!["long"]!, member_id: userId!, service_day: selectedDayString!, service_time: selectedTimeString!, image: selectedImage, video: selectedVideoUrl, downloadCompeted: { (res) in
            })
        }
    }
    
    func requestProduct() {
        let params: [String: Any] = [
            "store_id": selectedProduct!.storeId,
            "product_id": selectedProduct!.id,
            "userId": userId!,
            "address": "address",
            "map_lat": coords!["lat"]!,
            "map_lng": coords!["long"]!,
            "service_day": selectedDayString!,
            "service_time": selectedTimeString!,
            "quntity": amount.text!,
            "total_price": total.text!
        ]
        
        StoreManager.requestProduct(params: params) { res in
            print("product request successfull")
        }
    }
    
    @IBAction func sendRequest(_ sender: Any) {
        if requestType == "service" {
          requestService()
        } else {
          requestProduct()
        }
        
    }
    
    func validateForm() -> Bool {
        if categoryId != nil && userId != nil && selectedDayString != nil && selectedTimeString != nil && self.coords != nil {
            print("form valid")
            return true
        } else {
            print("form invalid")
            return false
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let mediaType = info[UIImagePickerControllerMediaType] as? String{
            
            if mediaType.isEqual(kUTTypeMovie as String) {
                if let mediaURL = info[UIImagePickerControllerMediaURL] as? URL {
                   print(mediaURL)
                    self.selectedVideoUrl = mediaURL
                    Utils.captureThumbnail(withVideoURL: mediaURL, completionHandler: { (res) in
                        if let image = res {
                            self.selectedVideoThumb.image = image
                        }
                    })
                }
            } else if mediaType.isEqual(kUTTypeImage as String){
                if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    print(image)
                    self.selectedImage = image
                    self.selectedImageThumb.image = image
                }
            }
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func selectMedia(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        if (sender.tag == 73) {
            // Add photo
            imagePicker.mediaTypes = [kUTTypeImage as String]
        } else {
            // Add video
            imagePicker.mediaTypes = [kUTTypeMovie as String]
        }
        present(imagePicker, animated: true, completion: nil)
    }

    
    func textFieldDidChange(_ textField: MFTextField) {
        if (textField.text != nil && textField.text != "") {
            print(textField.text!)
            let calc = Int(textField.text!)! * Int((selectedProduct?.price)!)!
            total.text = "\(calc)"
        }
        
    }
    
}
