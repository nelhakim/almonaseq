//
//  RegisterVC.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/9/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit
import MaterialTextField
import LocationPicker
import CoreLocation

class RegisterVC: UITableViewController, CLLocationManagerDelegate {
    let locationPicker = LocationPickerViewController()
    let geocoder = CLGeocoder()
    let locationManager = CLLocationManager()
    var coords: [String:Double]?
   

    @IBOutlet weak var address: MFTextField!
  
    @IBOutlet weak var fullName: MFTextField!
    
    @IBOutlet weak var userName: MFTextField!
    
    @IBOutlet weak var email: MFTextField!
    
    @IBOutlet weak var mobile: MFTextField!
    
    @IBOutlet weak var password: MFTextField!
    @IBOutlet weak var registerBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        // Prevent keyboard from opening
        // for location field
        address.inputView = UIView()

        setupLocationPicker()
        
       
        fullName.errorFont = UIFont(name: "Avenir", size: 10)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

  
    @IBAction func register(_ sender: Any) {
        if (fullName.text?.isEmpty)! {
            let error = NSError(domain: "", code: 1, userInfo: ["fullname": "fullname"])
            fullName.setError(error, animated: true)
        } else {
            UserManager.register(name: fullName.text!, username: userName.text!, password: password.text!, email: email.text!, mobile: mobile.text!, lat: (self.coords?["lat"])!, long: (self.coords?["long"])!) { (res) in
                let registered = res["registered"] as! Bool
                if (registered) {
                    print("registration successful")
                    Utils.showToast(message: "registration successfull")
                    _ = self.navigationController?.popViewController(animated: true)
                }
                
            }
        }
        
    }

    @IBAction func chooseLocation(_ sender: MFTextField) {
        print("choose location")
        
        locationPicker.completion = { location in
            // do some awesome stuff with location
            print("location is\(String(describing: location?.address)) - ")
            self.address.text = location?.address
            self.coords = ["lat": (location?.coordinate.latitude)!, "long": (location?.coordinate.longitude)!]
            print("coords: \(String(describing: self.coords))")
        }
        
        navigationController?.pushViewController(locationPicker, animated: true)

    }
    
     // Setting location picker
    func setupLocationPicker() {
        
        // Get current coords
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestWhenInUseAuthorization()
//        locationManager.startMonitoringSignificantLocationChanges()
//    
//        let location: CLLocation = locationAuthStatus()
//        print("current location \(location.coordinate.latitude) - \(location.coordinate.longitude)")
//
//        geocoder.reverseGeocodeLocation(location) { (placemarks, errors) in
//        let initialLocation = Location(name: "My home", location: location, placemark: (placemarks?[0])!)
//        self.locationPicker.location = initialLocation
//        }
//

        locationPicker.mapType = .standard // default: .Hybrid
    }
    
    func locationAuthStatus() -> CLLocation {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            let currentLocation = locationManager.location
            return currentLocation!
//            Location.sharedInstance.latitude = currentLocation.coordinate.latitude
//            Location.sharedInstance.longitude = currentLocation.coordinate.longitude
//            print("longitude", Location.sharedInstance.longitude)
         
        } else {
            locationManager.requestWhenInUseAuthorization()
            return locationAuthStatus()
        }
    }
    
    func addValidators() {
        
    }
}
