//
//  provider.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/14/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import Foundation
class Provider {
    var _id: String!
    var _name: String!
    var _englishName: String!
    var _status: Bool!
    var _price: Float!
    var _imageUrl: String!
    var _rating: Float!
    var _providerDescription: String!
    var _comments: Int!
    
    var id: String {
        if _id == nil {
            return ""
        }
        return _id
    }
    
    var name: String {
        if _name == nil {
            return ""
        }
        return _name
    }
    
    var englishName: String {
        if _englishName == nil {
            return ""
        }
        return _englishName
    }
    
    var status: Bool {
        if _status == nil {
            return false
        }
        return _status
    }
    
    var price: Float {
        if _price == nil {
            return 0
        }
        return _price
    }
    
    
    var imageUrl: String {
        if _imageUrl == nil {
            return ""
        }
        return _imageUrl
    }
    
    var rating: Float {
        if _rating == nil {
            return 0
        }
        return _rating
    }
    
    var providerDescription: String {
        if _providerDescription == nil {
            return ""
        }
        return _providerDescription
    }
    var comments: Int {
        if _comments == nil {
            return 0
        }
        return _comments
    }
    
    init(id: String, name: String, englishName: String, status: Bool, price: Float, imageUrl: String, rating: Float) {
        _id = id
        _name = name
        _englishName = englishName
        _status = status
        _price = price
        _imageUrl = imageUrl
        _rating = rating
    }
    
    init(id: String, name: String, englishName: String, status: Bool, price: Float, imageUrl: String, rating: Float, providerDescription:String, comments: Int) {
        _id = id
        _name = name
        _englishName = englishName
        _status = status
        _price = price
        _imageUrl = imageUrl
        _rating = rating
        _providerDescription = providerDescription
        _comments = comments
    }
    
    
}
