//
//  LoginVC.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/9/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit
import MaterialTextField
class LoginVC: UITableViewController {

    @IBOutlet weak var email: MFTextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var password: MFTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let imageView = UIImageView(image: UIImage(named: "bg"))
        self.tableView.backgroundView = imageView
//        _ = Utils.initializeGradient(view: loginBtn)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    @IBAction func login(_ sender: Any) {
        loginUser()
    }
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func loginUser() {
        if (!(email.text?.isEmpty)! && !(password.text?.isEmpty)!) {
            print("logging in")
            UserManager.login(email: email.text!, password: password.text!, downloadCompeted: { (res) in
                print("login success")
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
}
