//
//  ForgotPasswordVC.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 12/25/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UITableViewController {

    @IBOutlet weak var emailStack: UIStackView!
    @IBOutlet weak var mobileStack: UIStackView!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet var emailMobileToggle: [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()

        Utils.removeGradientLayer(view: emailMobileToggle[1])
        emailMobileToggle[1].setTitleColor(UIColor.black, for: .normal)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

  
    
    @IBAction func toggleEmailMobile(_ sender: UIButton) {
        emailMobileToggle.forEach { (button) in
            Utils.removeGradientLayer(view: button)
            button.setTitleColor(UIColor.black, for: .normal)
        }
        print("tapped\(sender.tag)")
         Utils.addGradientLayer(view: sender)
        sender.setTitleColor(UIColor.white, for: .normal)
        if (sender.tag == 1) {
            emailStack.isHidden = true
            mobileStack.isHidden = false
        } else {
            mobileStack.isHidden = true
            emailStack.isHidden = false
        }
    }

}
