//
//  StoreCell.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 1/18/18.
//  Copyright © 2018 Nada El Hakim. All rights reserved.
//

import UIKit

class StoreCell: UICollectionViewCell {

    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var storeImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        UIView.circularView(view: storeImage)
    }
    
   
    
    func configureCell(store: Store) {
        name.text = store.name
       
    storeImage.sd_setImage(with: URL(string: store.image), placeholderImage: UIImage(named: "logo"))
    }

}
