//
//  ApiManager.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/9/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Toaster
import KVSpinnerView


class ApiManager {
    
    
    static func get(url: String, method: HTTPMethod = .get, params: [String: Any] = [:], showHUD: Bool = true, onSuccess: @escaping OnSuccess) {
        print(url)
        
        if (showHUD) {
            KVSpinnerView.show()
        }
        
        Alamofire.request(
            url,
            method: method,
            parameters: params,
            headers: [:]).validate().responseJSON { response in
                
                switch response.result {
                case .success(let value):
                    onSuccess(JSON(value))
                                    print(JSON(value))
                case .failure(let error):
                    print("error")
                    debugPrint("error")
                    print(error)
                }
                if (showHUD) {
                    KVSpinnerView.dismiss()
                }
        }
    }
    
    
}

