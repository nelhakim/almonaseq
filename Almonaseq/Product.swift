//
//  Product.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 12/18/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import Foundation
class Product {
    private var _id: String!
    private var _storeId: String!
    private var _name: String!
    private var _englishName: String!
    private var _price: String!
    private var _image: String!
    
    var id: String {
        if  _id == nil {
            _id = ""
        }
        return _id
    }
    var storeId: String {
        get {
            if  _storeId == nil {
                _storeId = ""
            }
            return _storeId
        }
        set(newValue) {
            _storeId = newValue
        }
        
    }
    
    var name: String {
        if  _name == nil {
            _name = ""
        }
        return _name
    }
    
    var englishName: String {
        if  _englishName == nil {
            _englishName = ""
        }
        return _englishName
    }
    
    var price: String {
        if  _price == nil {
            _price = ""
        }
        return _price
    }
    
    var image: String {
        if  _image == nil {
            _image = ""
        }
        return _image
    }
    
    init(id: String, name: String, englishName: String, price: String, image: String) {
        _id = id
        _name = name
        _englishName = englishName
        _price = price
        _image = image
    }
}
