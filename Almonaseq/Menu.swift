//
//  menu.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/17/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import Foundation
class Menu {
    var _name: String
    var _englishName: String
    var _icon: String
    var _vc: String

    var icon: String {
        return _icon
    }
    
    var englishName: String {
        return _englishName
    }
    
    var name: String {
        return _name
    }
    
    var vc: String {
        return _vc
    }
    
    init(name: String, englishName: String, icon: String, vc: String) {
        _name = name
        _englishName = englishName
        _icon = icon
        _vc = vc
    }
}
