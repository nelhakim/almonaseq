//
//  ProductsVC.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 1/18/18.
//  Copyright © 2018 Nada El Hakim. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ProductCell"

class ProductsVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var storeId: String?
    var products = [Product]()
    let userDef = UserDefaults.standard
    var userId: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageView = UIImageView(image: UIImage(named: "bg"))
        self.collectionView?.backgroundView = imageView
        userId = userDef.string(forKey: "user_id")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView?.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        }

    override func viewWillAppear(_ animated: Bool) {
        getStoreProducts()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? ProductCell {
            let currentProduct = products[indexPath.row]
            cell.configureCell(product: currentProduct)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedProduct = products[indexPath.row]
        var storyBoard: UIStoryboard
        if (storeId != nil) {
            selectedProduct.storeId = storeId!
        }
        if userId != nil {
            storyBoard = UIStoryboard(name: "Store", bundle: nil)
            if let vc = storyboard?.instantiateViewController(withIdentifier: "RequestVC") as? RequestVC {
                vc.userId = userId
                vc.selectedProduct = selectedProduct
                vc.requestType = "product"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            storyBoard = UIStoryboard(name: "Auth", bundle: nil)
            if let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
                //                self.navigationController?.pushViewController(vc, animated: true)
                self.present(vc, animated: true, completion: nil)
            }
        }
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LogoHeader", for: indexPath)
        return headerView
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize = CGSize()
        let  collectionWidth: CGFloat = (self.collectionView?.frame.size.width)!
        let itemWidth: CGFloat = (collectionWidth / 2) - 26
        let itemHeight: CGFloat = itemWidth * 0.8
        cellSize = CGSize(width: itemWidth, height: itemHeight)
        return cellSize
        
    }
    
    func getStoreProducts() {
        if (storeId != nil) {
            StoreManager.getStoreProducts(id: storeId!, downloadCompeted: { (products) in
                self.products = products
                self.collectionView?.reloadData()
            })
        }
    }

}
