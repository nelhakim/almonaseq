//
//  menuItems.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/17/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import Foundation
let menuItems: [Menu] = [
    Menu(name: "", englishName: "Home", icon: "home", vc: "ServicesVC"),
    Menu(name: "", englishName: "Materials List", icon: "home", vc: "StoresVC"),
    Menu(name: "", englishName: "Profile", icon: "user", vc: "StoresVC"),
    Menu(name: "", englishName: "Notifications", icon: "notification", vc: "NotificationsVC"),
    Menu(name: "", englishName: "My Orders", icon: "cart", vc: "OrdersVC"),
    Menu(name: "", englishName: "About us", icon: "information", vc: "AboutVC"),
    Menu(name: "", englishName: "Common Questions", icon: "lightbulb", vc: "FaqsVC"),
    Menu(name: "", englishName: "Settings", icon: "cog", vc: "SettingsVC"),
    Menu(name: "", englishName: "Sign in", icon: "home", vc: "LoginVC")
]
