//
//  utils.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/17/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit
import Toaster
import AVFoundation
class Utils {
    static var gradientLayer: CAGradientLayer!
    static func initializeGradient() -> CALayer {
        gradientLayer = CAGradientLayer()
        gradientLayer.colors = [gradientColor1, gradientColor2]
        gradientLayer.locations = [0.0, 0.5]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        view.layer.addSublayer(gradientLayer)
        return gradientLayer
    }
    
    static func addGradientLayer(view: UIView) {
        let gradientLayer = initializeGradient()
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    static func removeGradientLayer(view: UIView) {
        if ((view.layer.sublayers?.count)! > 0) {
            view.layer.sublayers?.forEach { layer in
                if layer.isKind(of: CAGradientLayer.self) {
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
    
    
    static func showToast(message: String) {
        let toast = Toast(text: message, duration: Delay.long)
        toast.show()
    }
    
    static func captureThumbnail(withVideoURL videoURL:URL,secs:Int = 10,preferredTimeScale scale:Int = 1,completionHandler:((UIImage?) ->Void)?) -> Void
    {
        //let seconds : Int64 = 10
        // let preferredTimeScale : Int32 = 1
        
        DispatchQueue.global().async {
            
            do
            {
                let asset = AVURLAsset(url: videoURL)
                
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at:CMTimeMake(Int64(secs), Int32(scale)),actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                completionHandler?(thumbnail)
            }
            catch let error as NSError
            {
                print("Error generating thumbnail: \(error)")
                completionHandler?(nil)
            }
        }
    }
}
