//
//  SideMenuVC.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/9/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit

/*
 Menu controller is responsible for creating its content and showing/hiding menu using 'menuContainerViewController' property.
 */
class SideMenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let kCellReuseIdentifier = "MenuCell"
    var cellHeight: CGFloat = 60.0
    var menuItemsArray: [Menu] = menuItems
    

    
    @IBOutlet weak var tableView: UITableView!
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Utils.initializeGradient(view: gradientLayer)
        print("table view loaded")

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName:kCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: kCellReuseIdentifier)
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(menuItems.count)
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("adding cells")
        if let cell = tableView.dequeueReusableCell(withIdentifier: kCellReuseIdentifier, for: indexPath) as? MenuCell {
            //cell.backgroundColor = UIColor.clear
            
            cell.configureCell(menu: menuItemsArray[indexPath.row])
//            cell.menuItemTitle.text = menuItemsArray[indexPath.row].englishName
            //cell.menuItemTitle.textColor = UIColor.white
            cell.selectionStyle = .none
            return cell
        } else {
            print("empty cells")
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let identifier = menuItems[indexPath.row].vc
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: identifier) {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
//        guard let menuContainerViewController = self.menuContainerViewController else {
//            return
//        }
//
//        menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[indexPath.row])
//        menuContainerViewController.hideSideMenu()
    }
   
}
