//
//  config.swift
//  Almonaseq
//
//  Created by Nada El Hakim on 11/17/17.
//  Copyright © 2017 Nada El Hakim. All rights reserved.
//

import UIKit
import SwiftyJSON


// App colors
// Green
let primaryColor = UIColor(red:0.00, green:0.51, blue:0.09, alpha:1.0)
//Blue
let secondaryColor = UIColor(red:0.00, green:0.37, blue:0.59, alpha:1.0)
let gradientColor1: CGColor = secondaryColor.cgColor
let gradientColor2: CGColor = primaryColor.cgColor



typealias OnSuccess = (_: JSON) -> ()
typealias DownloadComplete<T> = (_: T) -> ()
typealias Failure<T> = (_: T) -> ()

class Constants {
    // Web services
    
    static let API_ENDPOINT = "http://monasiq.com/monsq/mobile/"
    static let CATEGORIES_URL: String = API_ENDPOINT + "cats/"
    static let REGISTER_ENDPOINT: String = API_ENDPOINT + "user_register/"
    static let LOGIN_ENDPOINT: String = API_ENDPOINT + "login/"
    static let STORE_ENDPOINT: String = API_ENDPOINT + "stors/"
    static let STORE_PRODUCTS_ENDPOINT: String = API_ENDPOINT + "stors_products/"
    static let REQUEST_SERVICE_ENDPOINT: String = API_ENDPOINT +
    "service_request/"
    static let REQUEST_PRODUCT_ENDPOINT: String = API_ENDPOINT + "product_request/"

}
